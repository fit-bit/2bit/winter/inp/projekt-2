-- cpu.vhd: Simple 8-bit CPU (BrainF*ck interpreter)
-- Copyright (C) 2019 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): Matěj Kudera (xkuder04)
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   -- ovládání automatu
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(12 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (0) / zapis (1)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is
 -- zde dopiste potrebne deklarace signalu

 ---------------------------- FSM -------------------------------------
 -- stavy konečného automatu ovládající procesor
 type fsm_state is (
   s_idle, -- Idle stav procesoru, procesor nic neprovácí
   s_halt, -- Zastavení vykonávání procesoru
   s_fetch0, -- Čtení instrukce z paměti
   s_fetch1, -- Zvíšení PC
   s_decode, -- Dekodování instrukce
   s_inc_ptr, -- inkrementace hodnoty ukazatele PTR
   s_dec_ptr, -- dekrementace hodnoty ukazatele PTR
   s_inc_val1, -- inkrementace hodnoty aktuálnı́ buňky na kterou ukazuje PTR
   s_inc_val2,
   s_dec_val1, -- dekrementace hodnoty aktuálnı́ buňky na kterou ukazuje PTR
   s_dec_val2,
   s_jmp_zero1, -- je-li hodnota aktuálnı́ buňky na kterou ukazuje PTR nulová, skoč za odpovı́dajı́cı́ přı́kaz ] jinak pokračuj následujı́cı́m znakem
   s_jmp_zero2,
   s_jmp_zero3,
   s_jmp_zero4,
   s_jmp_not_zero1, -- je-li hodnota aktuálnı́ buňky na kterou ukazuje PTR nenulová, skoč za odpovı́dajı́cı́ přı́kaz [ jinak pokračuj následujı́cı́m znakem
   s_jmp_not_zero2,
   s_jmp_not_zero3,
   s_jmp_not_zero4,
   s_jmp_not_zero5,
   s_jmp_not_zero6,
   s_print_val1, -- vytiskni hodnotu buňky na kterou ukazuje PTR na display
   s_print_val2,
   s_load_val1, -- načti hodnotu z klávesnice a ulož ji do buňky na kterou ukazuje PTR
   s_load_val2,
   s_store_to_tmp1, -- ulož hodnotu buňky na kterou ukazuje PTR do pomocné proměnné
   s_store_to_tmp2,
   s_load_from_tmp1, -- hodnotu pomocné proměnné ulož do buňky na kterou ukazuje PTR
   s_load_from_tmp2); 

  -- Sygnály pro stav procesoru
  signal present_state : fsm_state;
  signal next_state : fsm_state;

 ---------------------------- Register PC -------------------------------------
  signal pc_reg : std_logic_vector(12 downto 0);
  signal pc_inc : std_logic;
  signal pc_dec : std_logic;

 ---------------------------- Register PTR -------------------------------------
 signal ptr_reg : std_logic_vector(12 downto 0);
 signal ptr_inc : std_logic;
 signal ptr_dec : std_logic;
 signal ptr_set_max : std_logic;
 signal ptr_set_min : std_logic;

 ---------------------------- Register CNT -------------------------------------
 signal cnt_reg : std_logic_vector(7 downto 0);
 signal cnt_set_one : std_logic;
 signal cnt_inc : std_logic;
 signal cnt_dec : std_logic;

 ---------------------------- Multiplexor MX1 -------------------------------------
 signal mx1_sel : std_logic;
 signal mx1_op1 : std_logic_vector(12 downto 0);
 signal mx1_op2 : std_logic_vector(12 downto 0);

 ---------------------------- Multiplexor MX2 -------------------------------------
 signal mx2_sel : std_logic;
 signal mx2_op1 : std_logic_vector(12 downto 0);

 ---------------------------- Multiplexor MX3 (ALU) -------------------------------------
 type mx3_oper_type is (alu_pass_from_io, alu_dec, alu_inc, alu_pass_from_ram);
 signal mx3_sel : mx3_oper_type;

 ---------------------------- Dekodér -------------------------------------
  type instr_type is (
    inc_ptr, -- 0x3E
    dec_ptr, -- 0x3C
    inc_val, -- 0x2B
    dec_val, -- 0x2D
    jmp_zero, -- 0x5B
    jmp_not_zero, -- 0x5D
    print_val, -- 0x2E
    load_val, -- 0x2C
    store_to_tmp, -- 0x24
    load_from_tmp, -- 0x21
    halt, -- 0x00
    pass); -- P�esko�en� koment��e
  signal instr: instr_type;

 --------------------------- Test CNT a DATA_RDATA na rovnost s 0 --------------
  signal cnt_zero : std_logic;
  signal rdata_zero : std_logic;

begin
 -- zde dopiste vlastni VHDL kod

 -- pri tvorbe kodu reflektujte rady ze cviceni INP, zejmena mejte na pameti, ze 
 --   - nelze z vice procesu ovladat stejny signal,
 --   - je vhodne mit jeden proces pro popis jedne hardwarove komponenty, protoze pak
 --   - u synchronnich komponent obsahuje sensitivity list pouze CLK a RESET a 
 --   - u kombinacnich komponent obsahuje sensitivity list vsechny ctene signaly.

---------------------------------- FSM ---------------------------------------------
-- Řadič činnosti procesoru

 FSM: process (present_state, instr, ptr_reg ,cnt_zero, rdata_zero, DATA_RDATA, OUT_BUSY, IN_VLD)
 begin
  -- Inicializace signálů
  DATA_RDWR <= '0';
  DATA_EN <= '0';
  OUT_WE <= '0';
  IN_REQ <= '0';
  mx1_sel <= '0';
  mx2_sel <= '0';
  mx3_sel <= alu_pass_from_io;
  pc_inc <= '0';
  pc_dec <= '0';
  ptr_inc <= '0';
  ptr_dec <= '0';
  ptr_set_max <= '0';
  ptr_set_min <= '0';
  cnt_set_one <= '0';
  cnt_inc <= '0';
  cnt_dec <= '0';

  -- Case přechodů
  case present_state is

    -- Procesor v idle
    when s_idle =>
      next_state <= s_fetch0;

    -- Načtení instrukce FETCH
    when s_fetch0 =>
      mx1_sel <= '0';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_fetch1;

    when s_fetch1 =>
      pc_inc <= '1';
      next_state <= s_decode;

    -- Deódování instrukce
    when s_decode =>
      case instr is
        when inc_ptr => 
          next_state <= s_inc_ptr;

        when dec_ptr => 
          next_state <= s_dec_ptr;

        when inc_val =>
          next_state <= s_inc_val1;

        when dec_val => 
          next_state <= s_dec_val1;

        when jmp_zero => 
          next_state <= s_jmp_zero1;

        when jmp_not_zero => 
          next_state <= s_jmp_not_zero1;

        when print_val =>  
          next_state <= s_print_val1;

        when load_val => 
          next_state <= s_load_val1;

        when store_to_tmp => 
          next_state <= s_store_to_tmp1;

        when load_from_tmp => 
          next_state <= s_load_from_tmp1;

	when pass => 
          next_state <= s_fetch0;

        when others => -- Špatná instrukce procesoru
          next_state <= s_halt;
      end case;

    -- inkrementace hodnoty ukazatele PTR
    when s_inc_ptr =>
      if (ptr_reg = "1111111111111") then
	ptr_set_min <= '1';
      	next_state <= s_fetch0;
      else
      	ptr_inc <= '1';
      	next_state <= s_fetch0;
     end if;

    -- dekrementace hodnoty ukazatele PTR
    when s_dec_ptr =>
      if (ptr_reg = "1000000000000") then
	ptr_set_max <= '1';
      	next_state <= s_fetch0;
      else
     	ptr_dec <= '1';
      	next_state <= s_fetch0;
      end if;
    
    -- inkrementace hodnoty aktuálnı́ buňky na kterou ukazuje PTR
    when s_inc_val1 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_inc_val2;

    when s_inc_val2 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      mx3_sel <= alu_inc;
      DATA_RDWR <= '1';
      DATA_EN <= '1';
      next_state <= s_fetch0;
      
    -- dekrementace hodnoty aktuálnı́ buňky na kterou ukazuje PTR
    when s_dec_val1 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_dec_val2;

    when s_dec_val2 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      mx3_sel <= alu_dec;
      DATA_RDWR <= '1';
      DATA_EN <= '1';
      next_state <= s_fetch0;

    -- je-li hodnota aktuálnı́ buňky na kterou ukazuje PTR nulová, skoč za odpovı́dajı́cı́ přı́kaz ] jinak pokračuj následujı́cı́m znakem
    when s_jmp_zero1 => 
      mx2_sel <= '0';
      mx1_sel <= '1';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_jmp_zero2;

    when s_jmp_zero2 => -- test mem[ptr] == 0
      if (rdata_zero = '1') then
        cnt_set_one <= '1';
        next_state <= s_jmp_zero3;
      else
        next_state <= s_fetch0;
      end if;

    when s_jmp_zero3 => -- while cnt != 0
      if (cnt_zero = '1') then
        next_state <= s_fetch0;
      else
        mx1_sel <= '0';
        DATA_RDWR <= '0';
        DATA_EN <= '1';
        next_state <= s_jmp_zero4;
      end if;

    when s_jmp_zero4 => -- je to [ nebo ] 
      if (DATA_RDATA = "01011011") then
        cnt_inc <= '1';
        pc_inc <= '1';
        next_state <= s_jmp_zero3;
      elsif (DATA_RDATA = "01011101") then
        cnt_dec <= '1';
        pc_inc <= '1';
        next_state <= s_jmp_zero3;
      else
        pc_inc <= '1';
        next_state <= s_jmp_zero3;
      end if;

    -- je-li hodnota aktuálnı́ buňky na kterou ukazuje PTR nenulová, skoč za odpovı́dajı́cı́ přı́kaz [ jinak pokračuj následujı́cı́m znakem
    when s_jmp_not_zero1 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_jmp_not_zero2;
    
    when s_jmp_not_zero2 => -- test mem[ptr] == 0
      if (rdata_zero = '1') then
        next_state <= s_fetch0;
      else
        cnt_set_one <= '1';
        pc_dec <= '1';
        next_state <= s_jmp_not_zero3;
      end if;

    when s_jmp_not_zero3 => -- PC musím odečíst 2x, protože ho vždy automaticky přičtu
      pc_dec <= '1';
      next_state <= s_jmp_not_zero4;

    when s_jmp_not_zero4 => -- while cnt != 0
      if (cnt_zero = '1') then
        next_state <= s_fetch0;
      else
        mx1_sel <= '0';
        DATA_RDWR <= '0';
        DATA_EN <= '1';
        next_state <= s_jmp_not_zero5;
      end if;

    when s_jmp_not_zero5 => -- je to [ nebo ]
      if (DATA_RDATA = "01011011") then
        cnt_dec <= '1';
        next_state <= s_jmp_not_zero6;
      elsif (DATA_RDATA = "01011101") then
        cnt_inc <= '1';
        next_state <= s_jmp_not_zero6;
      else
        next_state <= s_jmp_not_zero6;
      end if;
      
    when s_jmp_not_zero6 => -- cnt == 0
      if (cnt_zero = '1') then
        pc_inc <= '1';
        next_state <= s_jmp_not_zero4;
      else
        pc_dec <= '1';
        next_state <= s_jmp_not_zero4;
      end if;
    
    -- vytiskni hodnotu buňky na kterou ukazuje PTR na display 
    when s_print_val1 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_print_val2;
        
    when s_print_val2 =>
      if (OUT_BUSY = '1') then
        next_state <= s_print_val2;
      else
        OUT_WE <= '1';
      	next_state <= s_fetch0;
      end if;

    -- načti hodnotu z klávesnice a ulož ji do buňky na kterou ukazuje PTR
    when s_load_val1 =>
      IN_REQ <= '1';
      next_state <= s_load_val2;

    when s_load_val2 =>
      if (IN_VLD = '0') then
	IN_REQ <= '1';
        next_state <= s_load_val2;
      else
        mx2_sel <= '0';
        mx1_sel <= '1';
        mx3_sel <= alu_pass_from_io;
        DATA_RDWR <= '1';
        DATA_EN <= '1';
        next_state <= s_fetch0;
      end if;

    -- ulož hodnotu buňky na kterou ukazuje PTR do pomocné proměnné
    when s_store_to_tmp1 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_store_to_tmp2;

    when s_store_to_tmp2 =>
      mx2_sel <= '1';
      mx1_sel <= '1';
      mx3_sel <= alu_pass_from_ram;
      DATA_RDWR <= '1';
      DATA_EN <= '1';
      next_state <= s_fetch0;

    -- hodnotu pomocné proměnné ulož do buňky na kterou ukazuje PTR
    when s_load_from_tmp1 =>
      mx2_sel <= '1';
      mx1_sel <= '1';
      DATA_RDWR <= '0';
      DATA_EN <= '1';
      next_state <= s_load_from_tmp2;

    when s_load_from_tmp2 =>
      mx2_sel <= '0';
      mx1_sel <= '1';
      mx3_sel <= alu_pass_from_ram;
      DATA_RDWR <= '1';
      DATA_EN <= '1';
      next_state <= s_fetch0;

    when s_halt =>
      null;

    when others =>
      null;
  end case;
 end process;

 -- FSM, přesun do dalšího stavu 
 FSM_next_state: process (RESET, CLK)
 begin
  if (RESET = '1') then
    present_state <= s_idle;
  elsif (CLK'event) and (CLK='1') then
    if (EN = '1') then
      present_state <= next_state;
    end if;
  end if;
 end process;

---------------------------- Register PC -------------------------------------
-- Ukazatel na následující instrukci

  PC: process (RESET, CLK)
  begin
    if (RESET = '1') then
      pc_reg <= (others => '0');
    elsif (CLK'event) and (CLK = '1') then
      if (pc_inc = '1') then
        pc_reg <= pc_reg + 1;
      elsif (pc_dec = '1') then
          pc_reg <= pc_reg - 1;
      end if;
    end if;
  end process;

  mx1_op1 <= pc_reg;

---------------------------- Register PTR -------------------------------------
-- Ukazatel do paměti na data co chceme číst

PTR: process (RESET, CLK)
begin
  if (RESET = '1') then
    ptr_reg <= "1000000000000";
  elsif (CLK'event) and (CLK = '1') then
    if (ptr_inc = '1') then
      ptr_reg <= ptr_reg + 1;
    elsif (ptr_dec = '1') then
        ptr_reg <= ptr_reg - 1;
    elsif (ptr_set_max = '1') then
	ptr_reg <= "1111111111111";
    elsif (ptr_set_min = '1') then
	ptr_reg <= "1000000000000";	    
    end if;
  end if;
end process;

mx2_op1 <= ptr_reg;

---------------------------- Register CNT -------------------------------------
-- sloužı́ ke korektnı́mu určenı́ odpovı́dajı́cı́ho začátku/konce přı́kazu while

CNT: process (RESET, CLK)
begin
  if (RESET = '1') then
    cnt_reg <= (others => '0');
  elsif (CLK'event) and (CLK = '1') then
    if (cnt_inc = '1') then
      cnt_reg <= cnt_reg + 1;
    elsif (cnt_dec = '1') then
        cnt_reg <= cnt_reg - 1;
    elsif (cnt_set_one = '1') then
	cnt_reg <= "00000001";
    end if;
  end if;
end process;

---------------------------- Multiplexor MX1 -------------------------------------
-- Určuje jestli se z paměti čte instrukce nebo data

  with mx1_sel select
    DATA_ADDR <= mx1_op1 when '0',
                 mx1_op2 when others;

---------------------------- Multiplexor MX2 -------------------------------------
-- Umožnuje přečíst data s pomocné proměné TMP na adrese 0x1000

  with mx2_sel select
    mx1_op2 <= mx2_op1 when '0',
               "1000000000000" when others;

---------------------------- Multiplexor MX3 -------------------------------------
-- Vybírá co do paměti chceme zapsat, plní funkci ALU

with mx3_sel select
  DATA_WDATA <= IN_DATA when alu_pass_from_io,
                DATA_RDATA - 1 when alu_dec,
                DATA_RDATA + 1 when alu_inc,
                DATA_RDATA when others;

---------------------------- Dekodér -------------------------------------
-- Dekódování instrukce
-- inc_ptr, -- 0x3E
-- dec_ptr, -- 0x3C
-- inc_val, -- 0x2B
-- dec_val, -- 0x2D
-- jmp_zero, -- 0x5B
-- jmp_not_zero, -- 0x5D
-- print_val, -- 0x2E
-- load_val, -- 0x2C
-- store_to_tmp, -- 0x24
-- load_from_tmp, -- 0x21
-- halt); -- 0x00

  process (DATA_RDATA)
  begin
    case (DATA_RDATA(7 downto 4)) is
      when "0000" => -- 0x00
        instr <= halt;

      when "0010" =>
        case (DATA_RDATA(3 downto 0)) is
          when "0001" => -- 0x21
            instr <= load_from_tmp;

          when "0100" => -- 0x24
            instr <= store_to_tmp;

          when "1011" => -- 0x2B
            instr <= inc_val;

          when "1100" => -- 0x2C
            instr <= load_val;

          when "1101" => -- 0x2D
            instr <= dec_val;

          when "1110" => -- 0x2E
            instr <= print_val;

          when others =>
            instr <= pass;
        end case;

      when "0011" =>
        case (DATA_RDATA(3 downto 0)) is
          when "1100" => -- 0x3C
            instr <= dec_ptr;

          when "1110" => -- 0x3E
            instr <= inc_ptr;

          when others =>
            instr <= pass;
        end case;

      when "0101" =>
        case (DATA_RDATA(3 downto 0)) is
          when "1011" => -- 0x5B
            instr <= jmp_zero;

          when "1101" => -- 0x5D
           instr <= jmp_not_zero;

          when others =>
            instr <= pass;
        end case;

      when others =>
        instr <= pass;
    end case;
  end process;

--------------------------- Test CNT a DATA_RDATA na rovnost s 0 --------------
-- testy pro zanořené while a pro skok
  cnt_zero <= '1' when (cnt_reg = "00000000") else '0';
  rdata_zero <= '1' when (DATA_RDATA = "00000000") else '0';


  OUT_DATA <= DATA_RDATA;

end behavioral;
 
